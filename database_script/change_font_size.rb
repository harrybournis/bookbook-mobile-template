puts "Started #{'in destructive mode' if ARGV.include? '--destructive'}"
require "mysql2"

db_host  = "localhost"
db_user  = "root"
db_pass  = "root"
db_name = "bookbook"

client = Mysql2::Client.new(:host => db_host, :username => db_user, :password => db_pass, :database => db_name)
puts "Connected to database #{db_name}"

# Methods

# converts px to rem with 3 digit precision
def convert_px_to_rem(input)
	(input / 16.0).round 3
end

# substitutes pts and px on the given string. returns the converted string
def substitute(input)
	# change pts into rem
	result = input.gsub(/(?<=font-size:)\s*[0-9]+\.?[0-9]*pt;?/) do |result|
		case result.lstrip.chomp('pt;').to_i
		when 5
			' 0.418rem;'
		when 6
			' 0.501rem;'
		when 6.5
			' 0.543rem;'
		when 7
			' 0.563rem;'
		when 8
			' 0.688rem;'
		when 9
			' 0.750rem;'
		when 10
			' 0.813rem;'					
		when 11
			' 0.875rem;'
		when 12
			' 1rem;'
		when 14
			' 1.171rem;'
		else
			raise "unknown value for this pt. value received: #{result}"
		end					
	end	

	# change px into rem
	result = result.gsub(/(?<=font-size:)\s*[0-9]+\.?[0-9]*px;?/) do |result| 
		" #{convert_px_to_rem(result.lstrip.chomp('px;').to_i)}rem;" 
	end	
	result
end	


# Main
['j25_content', 'j25_k2_items'].each do |table|

	client.query("select * from #{table}").each do |r|
		puts "\n processing \"#{r['title']}\""

		['introtext', 'fulltext'].each do |field|
			temp = r[field]

			begin
				raise 'error in substituting' unless fixed = substitute(temp) 
				client.query("UPDATE `#{table}` SET `#{field}`='#{client.escape(fixed)}' where id = #{r['id']};")
			rescue
				client.query("UPDATE `#{table}` SET `#{field}`='#{client.escape(temp)}' where id = #{r['id']};")
			end

			unless ARGV.include? '--destructive'
				puts "\n reverting changes for #{field} \n"
				client.query("UPDATE `#{table}` SET `#{field}`='#{client.escape(temp)}' where id = #{r['id']};")
			end
		end
	end
end

puts "\n Finished."
