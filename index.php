<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">

  <?php include 'includes/head.php'; ?>
  <script type="text/javascript">
    // Use this to trigger bootstrap's affix, and fix random navbar position on reload
    (function ($) {
      $(window).ready(function () {
        $(window).scroll();
      });
    })(jQuery);
    // Remove all the damn joomla inline styles on images
    (function($){
        $(document).ready(function(){
           $(this).find('img').removeAttr("style");
        });
    })(jQuery);
  </script>
  <body id="body">

    <!-- Facebook -->
    <div id="fb-root"></div>
    <script>
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.async=true; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

    <?php include 'includes/header.php'; ?>

    <!-- wrapper for the sidenav and the content-->
    <div id="wrapper" class="wrapper">

      <?php include 'includes/sidenav.php'; ?>

      <!-- The wrapper of the content outside the sidenav -->
      <div id="page-content-wrapper">
        <!-- <div class="container">
          <div class="row">
            <div id="header-image" class="img-responsive"></div>
          </div>
        </div> -->
        <!-- wrapper for the main, unique content for each page -->
        <div class="container">
          <div id="header-image"></div>

          <jdoc:include type="modules" name="gamesCustomMenu"/>
          <jdoc:include type="modules" name="customAgeMenu"/>

          <!-- main content -->
          <jdoc:include type="component" />

          <div class="editors-choice-most-visited-row">
            <div class="editors-choice">
              <jdoc:include type="modules" name="editorsChoise" />
            </div> <!-- .editors-choice -->

            <div class="most-visited-article">
              <jdoc:include type="modules" name="mostVisitedArticle" />
            </div> <!-- .most-visited-article -->
          </div> <!--.editors-choice-most-visited-row-->

          <div class="banners">
            <jdoc:include type="modules" name="banners"/>
          </div> <!-- .banners -->

          <div class="custom-social-buttons">
            <jdoc:include type="modules" name="social-buttons"/>
          </div>

        </div> <!-- .container -->

        <!-- The dark overlay shown when side menu is open-->
        <div id="sidenav-overlay" class="hidden-animated"></div>

        <!--Used to drag the side menu from the side when on a mobile device-->
        <div id="drag-target" class="drag-target" data-sidenav="slide-out" style="touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); left: 0px;"></div>
      </div> <!-- #page-content-wrapper -->
    </div> <!-- #wrapper .wrapper -->

    <!-- Google Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-39557345-1', 'bookbook.gr');
      ga('require', 'displayfeatures');
      ga('require', 'linkid', 'linkid.js');
      ga('send', 'pageview');
    </script>  

    <!-- Quantcast Tag -->
    <script type="text/javascript">
    var _qevents = _qevents || [];
    (function() {
      var elem = document.createElement('script');
      elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
      elem.async = true;
      elem.type = "text/javascript";
      var scpt = document.getElementsByTagName('script')[0];
      scpt.parentNode.insertBefore(elem, scpt);
    })();
    _qevents.push({
      qacct:"p-Km6HqAzNaYfVK"
    });
    </script>

    <noscript>
      <div style="display:none;">
        <img src="//pixel.quantserve.com/pixel/p-Km6HqAzNaYfVK.gif" border="0" height="1" width="1" alt="Quantcast"/>
      </div>
    </noscript>
    <!-- End Quantcast tag --> 

    <!-- Alexa -->
    <div id="alexaStats">                        
      <a href="http://www.alexa.com/siteinfo/www.bookbook.gr"><script type='text/javascript' src='http://xslt.alexa.com/site_stats/js/s/a?url=www.bookbook.gr' ></script></a>     
    </div>      
  </body>
</html>
