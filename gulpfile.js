var gulp = require('gulp');

var config = require('./gulp-config.json');

// Dependencies
var beep        = require('beepbeep');
var browserSync = require('browser-sync');
var cleanCSS    = require('gulp-clean-css');
var concat      = require('gulp-concat');
// var del         = require('del');
var fs          = require('fs');
var gutil       = require('gulp-util');
var plumber     = require('gulp-plumber');
var rename      = require('gulp-rename');
var sass        = require('gulp-sass');
var uglify      = require('gulp-uglify');
var zip         = require('gulp-zip');
var xml2js      = require('xml2js');
var parser      = new xml2js.Parser();
var rimraf      = require('rimraf');

var extPath      = '.';
var assetsPath = '.';
var templateName = 'bookbookmobiletemplate';

var wwwPath = config.wwwDir + '/templates/' + templateName;

var templateFiles = [
	extPath + '/css/**',
	extPath + '/fonts/**',
	extPath + '/html/**',
	extPath + '/images/**',
	extPath + '/includes/**',
	extPath + '/js/**',
	extPath + '/scss/**',
	extPath + '/*.md',
	extPath + '/*.png',
	extPath + '/*.php',
	extPath + '/*.ico',
	extPath + '/*.xml'
];

var onError = function (err) {
    beep([0, 0, 0]);
    gutil.log(gutil.colors.green(err));
};

// Browsersync
gulp.task('browser-sync', function() {
	var browserConfig = {
		proxy : "joomla.box"
	}

	if (config.hasOwnProperty('browserConfig')) {
		browserConfig = config.browserConfig;
	}

    return browserSync(browserConfig);
});

// Clean
gulp.task('clean', function(cb) {
	rimraf.sync(wwwPath, { force: true }, cb);
});

// Copy
gulp.task('copy', ['sass', 'scripts', 'copy:css'], function() {
	console.log('copying');
	return gulp.src(templateFiles,{ base: extPath })
		.pipe(gulp.dest(wwwPath));
});

// Copies library css files
gulp.task('copy:css', function() {
	return gulp.src([
		extPath + '/scss/**/audioplayer/**',
		extPath + '/scss/**/imageviewer/**'
	])
	.pipe(gulp.dest(wwwPath + '/css'))
	.pipe(gulp.dest(assetsPath + '/css'));
});

function compileSassFile(src, destinationFolder, options)
{
	return gulp.src(src)
		.pipe(plumber({ errorHandler: onError }))
		.pipe(sass())
		.pipe(gulp.dest(assetsPath + '/' + destinationFolder))
		.pipe(gulp.dest(wwwPath + '/' + destinationFolder))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(rename(function (path) {
			path.basename += '.min';
		}))
		.pipe(gulp.dest(assetsPath + '/' + destinationFolder))
		.pipe(gulp.dest(wwwPath + '/' + destinationFolder));
}

// Sass
gulp.task('sass', function () {
	return compileSassFile(
		assetsPath + '/scss/template.scss',
		'css'
	);
});

function compileScripts(src, ouputFileName, destinationFolder) {
	return gulp.src(src)
		.pipe(plumber({ errorHandler: onError }))
		.pipe(concat(ouputFileName))
		.pipe(gulp.dest(extPath + '/' + destinationFolder))
		.pipe(gulp.dest(wwwPath + '/' + destinationFolder))
		.pipe(uglify())
		.pipe(rename(function (path) {
			path.basename += '.min';
		}))
		.pipe(gulp.dest(extPath + '/' + destinationFolder))
		.pipe(gulp.dest(wwwPath + '/' + destinationFolder));
}

// Minify scripts
gulp.task('scripts', function () {
	return compileScripts(
		[
			assetsPath + '/js/template.js'
		],
		'template.js',
		'js'
	);
});

// Watch
gulp.task('watch',
	[
		'watch:sass',
		'watch:scripts',
		'watch:template'
	],
	function() {
});

// Watch: template
gulp.task('watch:template',
	function() {
		var exclude = [
			'!' + extPath + '/css/**',
			'!' + extPath + '/scss/**',
			'!' + extPath + '/js/**',
		];

		gulp.watch(templateFiles.concat(exclude),['copy']);
});

// Watch: sass
gulp.task('watch:sass',
	function() {
		gulp.watch(
			extPath + '/scss/**',
			['sass']
		);
});

// Watch: scripts
gulp.task('watch:scripts',
	function() {
		gulp.watch([
			extPath + '/js/**/*.js'
			],
			['scripts']
		);
});

// Release
gulp.task('release', ['scripts', 'sass'],  function (cb) {
	fs.readFile(extPath + '/templateDetails.xml', function(err, data) {
		parser.parseString(data, function (err, result) {
			var version = result.extension.version[0];

			var fileName = result.extension.name + '-v' + version + '.zip';

			return gulp.src([...templateFiles, '!'+ extPath + '/scss/**'],{ base: extPath })
				.pipe(zip(fileName))
				.pipe(gulp.dest('releases'))
				.on('end', cb);
		});
	});
});

// Default task
gulp.task('default', ['clean', 'copy', 'watch'], function() {
	console.log('done');
});
