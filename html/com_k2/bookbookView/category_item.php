<?php
/**
 * @version		$Id: category_item.php 1689 2012-10-05 15:18:57Z  NOT lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>

<div itemscope itemtype="http://schema.org/Book" class="row catItemView group<?php echo ucfirst($this->item->itemGroup); ?><?php echo ($this->item->featured) ? ' catItemIsFeatured' : ''; ?><?php if ($this->item->params->get('pageclass_sfx')) echo ' ' . $this->item->params->get('pageclass_sfx'); ?>">
    <!-- Plugins: BeforeDisplay -->
    <?php echo $this->item->event->BeforeDisplay; ?>
    <!-- K2 Plugins: K2BeforeDisplay -->
    <?php echo $this->item->event->K2BeforeDisplay; ?>

    <!-- Plugins: AfterDisplayTitle -->
    <?php echo $this->item->event->AfterDisplayTitle; ?>
    <!-- K2 Plugins: K2AfterDisplayTitle -->
    <?php echo $this->item->event->K2AfterDisplayTitle; ?>

   <?php if ($this->item->params->get('catItemRating')): ?>
        <!-- Item Rating -->
        <div class="catItemRatingBlock">
            <span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
            <div class="itemRatingForm">
                <ul class="itemRatingList">
                    <li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
                </ul>
                <div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog">
                    <?php echo $this->item->numOfvotes; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="item_content">
        <!-- Plugins: BeforeDisplayContent -->
        <?php //echo $this->item->event->BeforeDisplayContent; ?>
        <!-- K2 Plugins: K2BeforeDisplayContent -->
        <?php //echo $this->item->event->K2BeforeDisplayContent; ?>

        <div class="top_content row">
                <div class="image_container col-xs-4 col-sm-3 col-md-2">
                    <?php if ($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
                        <a class="item-image-link" itemprop="url" href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>" title="<?php if (!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>">
                            <img class="img-responsive item-image" itemprop="image" src="<?php echo $this->item->image; ?>" alt="<?php if (!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>"/>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="info_container col-xs-8 col-sm-9 col-md-10">
                    <?php if ($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
                         <?php if ($this->item->params->get('catItemTitle')): ?>
                            <!-- Item title -->
                            <div id="title_view" itemprop="name">
                                <?php if (isset($this->item->editLink)): ?>
                                    <!-- Item edit link -->
                                    <span class="catItemEditLink">
                                        <a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->item->editLink; ?>">
                                            <?php echo JText::_('K2_EDIT_ITEM'); ?>
                                        </a>
                                    </span>
                                <?php endif; ?>
                                <!-- Capitalize titles of books.-->
                                <?php
                                $today = getdate();
                                $days = unixtojd($today[0]) - unixtojd(strtotime($this->item->created));
                                ?> 	 
                                <?php if ($days < 15): ?>
                                    <h4 class="title_txt">
                                        <a href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                            <?php echo mb_strtoupper($this->item->title); ?>
                                        </a>
                                    </h4>
                                    <?php
                                    if (($_GET[id] == '1') || ($_GET[id] == '324') || ($_GET[id] == '323') || ($_GET[id] == '22') ||
                                            ($_GET[id] == '24') || ($_GET[id] == '25') || ($_GET[id] == '26') || ($_GET[id] == '27')
                                            || ($_GET[id] == '23') || ($_GET[id] == '335') || ($_GET[id] == '336')):
                                        ?>
                                        <?php echo ("<img class='new_book' alt='Νέο βιβλίο' src='/images/books_icons/new_badge.png'"); ?>
                                    <?php endif; ?>	
                                <?php else: ?>
                                    <h4 class="title_txt">
                                        <a href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                            <?php echo mb_strtoupper($this->item->title); ?>
                                        </a>
                                    </h4>
                                <?php endif; ?>
                                <?php if ($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
                                    <!-- Featured flag -->
                                    <span>
                                        <sup>
                                            <?php echo JText::_('K2_FEATURED'); ?>
                                        </sup>
                                    </span>
                                <?php endif; ?>
                                <hr class="title_line">
                                <ul class="info_list">
                                    <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                                        <?php if ($extraField->value): ?>
                                            <div id="cat_info">
                                                <?php if ($extraField->name == "Συγγραφέας"): ?>
                                                    <li class="book_info author" itemprop="author">		
                                                        <?php if (strpos($extraField->value, 'href="http://"') !== false): ?>    										
                                                            <?php
                                                            $p = explode('&gt;', htmlspecialchars($extraField->value));
                                                            $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                            ?>
                                                            <?php if ($p[1] !== "http://"): ?> 
                                                            <strong>
                                                                <span class="extra_value author_value"><?php print_r($p[1]); ?></span>
                                                            </strong>    
                                                            <?php endif; ?>			
                                                        <?php else: ?>
                                                            <strong>
                                                                <span class="extra_value author_value"><?php echo $extraField->value; ?></span>	
                                                            </strong>	
                                                        <?php endif; ?>	
                                                    </li>
                                                <?php elseif ($extraField->name == "Εικονογράφηση"): ?>
                                                    <li class="book_info artwork">
                                                        <?php if (strpos($extraField->value, 'href="http://"') !== false): ?>    										
                                                            <?php
                                                            $p = explode('&gt;', htmlspecialchars($extraField->value));
                                                            $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                            ?>
                                                            <?php if ($p[1] !== "http://"): ?> 
                                                                <em>
                                                                    <?php echo "εικονογράφηση:"; ?> 
                                                                </em>
                                                                <strong>
                                                                    <?php print_r($p[1]); ?>
                                                                </strong>
                                                            <?php endif; ?>		
                                                        <?php else: ?>
                                                            <span class="extra_value artwork_val">
                                                                <?php
                                                                echo "εικονογράφηση:";
                                                                echo $extraField->value;
                                                                ?>
                                                            </span>		
                                                        <?php endif; ?>
                                                    </li>
                                                <?php elseif ($extraField->name == "Άλλο: Περιγραφή"): ?>
                                                    <li class="book_info other">
                                                        <?php foreach ($this->item->extra_fields as $key => $extraFieldSpecial): ?>
                                                            <?php if ($extraFieldSpecial->value): ?>
                                                                <?php if ($extraFieldSpecial->name == "Άλλο"): ?>

                                                                    <?php if (strpos($extraFieldSpecial->value, 'href="http://"') !== false): ?>    										
                                                                        <?php
                                                                        $p = explode('&gt;', htmlspecialchars($extraFieldSpecial->value));
                                                                        $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                                        ?>	
                                                                        <?php if ($p[1] !== "http://"): ?> 										
                                                                            <em>
                                                                                <?php echo $extraField->value; echo ": "; ?> 
                                                                            </em>
                                                                            <strong>
                                                                                <?php print_r($p[1]); ?>
                                                                            </strong> 
                                                                        <?php endif; ?>
                                                                    <?php else: ?>
                                                                        <em>	
                                                                            <?php
                                                                            echo $extraField->value;
                                                                            echo ": ";
                                                                            ?> 
                                                                        </em>
                                                                        <strong>
                                                                            <?php echo $extraFieldSpecial->value; ?>
                                                                        </strong>			
                                                                    <?php endif; ?>
                                                                <?php endif; ?>			
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </li>   
                                                <?php elseif ($extraField->name == "Εκδότης"): ?>
                                                    <li class="book_info publisher" itemprop="publisher">
                                                        <?php if (strpos($extraField->value, 'href="http://"') !== false): ?>    										
                                                            <?php
                                                            $p = explode('&gt;', htmlspecialchars($extraField->value));
                                                            $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                            ?>
                                                            <?php if ($p[1] !== "http://"): ?> 
                                                                <span class="extra_value publisher_value"><?php print_r($p[1]); ?></span>	
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <span class="extra_value publisher_value"><?php echo $extraField->value; ?></span>		
                                                        <?php endif; ?>
                                                    </li>
                                                <?php elseif ($extraField->name == "Ηλικία"): ?>
                                                    <li class="book_info age">
                                                        <?php if($extraField->value!="Χωρίς ηλικία"):?>
                                                            <span class="extra_value age_value"><?php echo $extraField->value; ?></span>
                                                        <?php endif; ?>			
                                                    </li>	
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if (($_GET[id] != '23') && ($_GET[id] != '24') && ($_GET[id] != '25') && ($_GET[id] != '26') && ($_GET[id] != '27')): ?>
                                        <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                                            <?php if ($extraField->value): ?>
                                                <?php if ($extraField->name == "Η γνώμη του bookbook.gr"): ?>
                                                    <?php if ($extraField->value != ""): ?>
                                                        <li class="badge_container read_it_badge">
                                                            <a class="badge" href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>#gnomiBookBookTitle">
                                                                <?php echo ("<img alt='Το διαβάσαμε' src='/images/books_icons/bookbook-we-ve-read-it_100px.jpg' />"); ?>
                                                            </a> 
                                                        </li>	
                                                    <?php endif; ?>	
                                                <?php endif; ?>			
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                                        <?php if ($extraField->value): ?>
                                            <?php if ($extraField->name == "Το αγαπήσαμε"): ?>
                                                <li class="badge_container loved_it_badge">
                                                    <?php if ($extraField->value == "Ναι"): ?>
                                                        <a class="badge" href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                                            <?php echo ("<img alt='Το αγαπήσαμε' src='/images/books_icons/bookbook-we-love_100px.jpg' />"); ?>		
                                                        </a> 
                                                    <?php endif; ?>
                                                </li>
                                            <?php endif; ?>			
                                        <?php endif; ?>
                                    <?php endforeach; ?> 

                                    <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                                        <?php if ($extraField->value): ?>
                                            <!-- Remove name from extra fields. Shows only the value.-->
                                            <!--<span class="catItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>-->
                                            <?php if ($extraField->name == "Δείτε το βιβλίο"): ?>								
                                                <?php
                                                if ((htmlspecialchars($extraField->value) != '&lt;a href=&quot;http://&quot; &gt;http://&lt;/a&gt;') &&
                                                        (htmlspecialchars($extraField->value) != "&lt;a href=&quot;http://&quot; class=&quot;modal&quot; rel=&quot;{handler:'iframe',size:{x:900,y:600}}&quot;&gt;http://&lt;/a&gt;") &&
                                                        (htmlspecialchars($extraField->value) != "&lt;a href=&quot;http://&quot; &gt;Ξεφυλλίστε τις πρώτες σελίδες του βιβλίου&lt;/a&gt;")
                                                ):
                                                    ?>
                                                    <?php if (strpos($extraField->value, 'href="http://"') == false): ?>  	
                                                        <li class="badge_container to_read_badge">
                                                            <?php $book_url = '<a href="'.$this->item->link.'&c_catid='.$_GET[id].'"'; ?>
                                                            <?php
                                                            $p = explode('&gt;', htmlspecialchars($extraField->value));
                                                            $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                            $p[1] = htmlspecialchars('<img alt="Ξεφυλλίστε το βιβλίο" src="/images/books_icons/bookbook-deite_100px.jpg" Title="Ξεφυλλίστε τις πρώτες σελίδες του βιβλίου"/>');
                                                            $p[1].='&lt;/a>';
                                                            echo htmlspecialchars_decode($book_url . 'class="badge">' . $p[1] . '' . $p[2]);
                                                            ?>	
                                                        </li>	
                                                    <?php endif; ?>	
                                                <?php endif; ?>							
                                            <?php endif; ?>	
                                        <?php endif; ?>
                                    <?php endforeach; ?>      

                                    <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                                        <?php if ($extraField->value): ?>
                                            <?php if ($extraField->name == "Απόσμασμα 1"): ?>								
                                                <?php if ((htmlspecialchars($extraField->value) != '')): ?>   
                                                    <li class="badge_container to_listen_badge">          						
                                                        <a class="badge" href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                                            <img alt='Ακόυστε το βιβλίο' src="/images/books_icons/listen_100px.png" />
                                                        </a>
                                                    </li>
                                                <?php endif; ?>							
                                            <?php endif; ?>	
                                        <?php endif; ?>
                                    <?php endforeach; ?> 
                                </ul>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
        </div>
    </div>

    <!-- Plugins: AfterDisplayContent -->
    <?php echo $this->item->event->AfterDisplayContent; ?>

    <!-- K2 Plugins: K2AfterDisplayContent -->
    <?php echo $this->item->event->K2AfterDisplayContent; ?>
    <hr class="divider">
</div>