 <div class="bottom_content">
            <?php if (($_GET[id] != '23') && ($_GET[id] != '24') && ($_GET[id] != '25') && ($_GET[id] != '26') && ($_GET[id] != '27')): ?>
                <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                    <?php if ($extraField->value): ?>
                        <?php if ($extraField->name == "Η γνώμη του bookbook.gr"): ?>
                            <?php if ($extraField->value != ""): ?>
                                <div class="badge_container read_it_badge">
                                    <a class="badge" href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>#gnomiBookBookTitle">
                                        <?php echo ("<img alt='Το διαβάσαμε' src='/images/books_icons/bookbook-we-ve-read-it_100px.jpg' />"); ?>
                                    </a> 
                                </div>	
                            <?php endif; ?>	
                        <?php endif; ?>			
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>	
        
            <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                <?php if ($extraField->value): ?>
                    <?php if ($extraField->name == "Το αγαπήσαμε"): ?>
                        <div class="badge_container loved_it_badge">
                            <?php if ($extraField->value == "Ναι"): ?>
                                <a class="badge" href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                    <?php echo ("<img alt='Το αγαπήσαμε' src='/images/books_icons/bookbook-we-love_100px.jpg' />"); ?>		
                                </a> 
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>			
                <?php endif; ?>
            <?php endforeach; ?> 

            <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                <?php if ($extraField->value): ?>
                    <!-- Remove name from extra fields. Shows only the value.-->
                    <!--<span class="catItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>-->
                    <?php if ($extraField->name == "Δείτε το βιβλίο"): ?>								
                        <?php
                        if ((htmlspecialchars($extraField->value) != '&lt;a href=&quot;http://&quot; &gt;http://&lt;/a&gt;') &&
                                (htmlspecialchars($extraField->value) != "&lt;a href=&quot;http://&quot; class=&quot;modal&quot; rel=&quot;{handler:'iframe',size:{x:900,y:600}}&quot;&gt;http://&lt;/a&gt;") &&
                                (htmlspecialchars($extraField->value) != "&lt;a href=&quot;http://&quot; &gt;Ξεφυλλίστε τις πρώτες σελίδες του βιβλίου&lt;/a&gt;")
                        ):
                            ?>
                            <?php if (strpos($extraField->value, 'href="http://"') == false): ?>  	
                                <div class="badge_container to_read_badge">
                                    <?php $book_url = '<a href="'.$this->item->link.'&c_catid='.$_GET[id].'"'; ?>
                                    <?php
                                    $p = explode('&gt;', htmlspecialchars($extraField->value));
                                    $p[1] = str_replace('&lt;/a', '', $p[1]);
                                    $p[1] = htmlspecialchars('<img alt="Ξεφυλλίστε το βιβλίο" src="/images/books_icons/bookbook-deite_100px.jpg" Title="Ξεφυλλίστε τις πρώτες σελίδες του βιβλίου"/>');
                                    $p[1].='&lt;/a>';
                                    echo htmlspecialchars_decode($book_url . 'class="badge">' . $p[1] . '' . $p[2]);
                                    ?>	
                                </div>	
                            <?php endif; ?>	
                        <?php endif; ?>							
                    <?php endif; ?>	
                <?php endif; ?>
            <?php endforeach; ?>      

            <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                <?php if ($extraField->value): ?>
                    <?php if ($extraField->name == "Απόσμασμα 1"): ?>								
                        <?php if ((htmlspecialchars($extraField->value) != '')): ?>   
                            <div class="badge_container to_listen_badge">          						
                                <a class="badge" href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                    <img alt='Ακόυστε το βιβλίο' src="/images/books_icons/listen_100px.png" />
                                </a>
                            </div>
                        <?php endif; ?>							
                    <?php endif; ?>	
                <?php endif; ?>
            <?php endforeach; ?> 
        </div>