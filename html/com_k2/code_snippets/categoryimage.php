<?php if($this->params->get('catImage') && $this->category->image): ?>
					<!-- Category image -->
					<img class="img category_image" alt="<?php echo K2HelperUtilities::cleanHtml($this->category->name); ?>" src="<?php echo $this->category->image; ?>" />
				<?php endif; ?>