<?php
/**
 * @version		$Id: category_item.php 1689 2012-10-05 15:18:57Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>

<!-- Start K2 Item Layout -->
<div itemscope itemtype="http://schema.org/Book" class="catItemView group<?php echo ucfirst($this->item->itemGroup); ?><?php echo ($this->item->featured) ? ' catItemIsFeatured' : ''; ?><?php if ($this->item->params->get('pageclass_sfx')) echo ' ' . $this->item->params->get('pageclass_sfx'); ?>">

    <!-- Plugins: BeforeDisplay -->
    <?php echo $this->item->event->BeforeDisplay; ?>

    <!-- K2 Plugins: K2BeforeDisplay -->
    <?php echo $this->item->event->K2BeforeDisplay; ?>


    <!-- Plugins: AfterDisplayTitle -->
    <?php echo $this->item->event->AfterDisplayTitle; ?>

    <!-- K2 Plugins: K2AfterDisplayTitle -->
    <?php echo $this->item->event->K2AfterDisplayTitle; ?>

    <?php if ($this->item->params->get('catItemRating')): ?>
        <!-- Item Rating -->
        <div class="catItemRatingBlock">
            <span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
            <div class="itemRatingForm">
                <ul class="itemRatingList">
                    <li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
                    <li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
                </ul>
                <div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
            </div>
            <div class="clr"></div>
        </div>
    <?php endif; ?>

    <div class="catItemBody">

        <!-- Plugins: BeforeDisplayContent -->
        <?php echo $this->item->event->BeforeDisplayContent; ?>

        <!-- K2 Plugins: K2BeforeDisplayContent -->
        <?php echo $this->item->event->K2BeforeDisplayContent; ?>

        <!-- Item Image -->
        <div class="catItemImageBlock">
            <div id="bookbookInfoTable">
                <table id="bookbookBookCatInfo" cellspacing="0" cellpadding="0" border="0" width="475">
                    <tr>
                        <td id="bookbookBookCatImage" align="left" valign="top" rowspan="4" width="80">
                            <div id="bookCatImage">
                                <?php if ($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
                                    <a itemprop="url" href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>" title="<?php if (!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>">
                                        <img  itemprop="image" src="<?php echo $this->item->image; ?>" alt="<?php if (!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>" style="width:<?php echo $this->item->imageWidth; ?>px; height:auto;" />
                                    </a>
                                <?php endif; ?>
                            </div>
                        </td>
                        <?php if ($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
                            <!-- Item extra fields -->		
                            <td valign="top" align="left" rowspan="4">
                                <!-- Cat item header placed next to the body in order for the title to be right from the book image -->
                                <div class="catItemHeader">
                                    <?php if ($this->item->params->get('catItemDateCreated')): ?>
                                        <!-- Date created -->
                                        <span class="catItemDateCreated">
                                            <?php echo JHTML::_('date', $this->item->created, JText::_('K2_DATE_FORMAT_LC2')); ?>
                                        </span>
                                    <?php endif; ?>

                                    <?php if ($this->item->params->get('catItemTitle')): ?>
                                        <!-- Item title -->
                                        <div id="customTitleCatItemView" itemprop="name">
                                            <?php if (isset($this->item->editLink)): ?>
                                                <!-- Item edit link -->
                                                <span class="catItemEditLink">
                                                    <a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->item->editLink; ?>">
                                                        <?php echo JText::_('K2_EDIT_ITEM'); ?>
                                                    </a>
                                                </span>
                                            <?php endif; ?>
                                            <!-- Capitalize titles of books.-->
                                            <?php
                                            $today = getdate();
                                            $days = unixtojd($today[0]) - unixtojd(strtotime($this->item->created));
                                            ?> 	 
                                            <?php if ($days < 15): ?>
                                                <h2>
                                                <a href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                                    <?php echo mb_strtoupper($this->item->title); ?>
                                                </a>
						</h2>
                                                <?php
                                                if (($_GET[id] == '1') || ($_GET[id] == '324') || ($_GET[id] == '323') || ($_GET[id] == '22') ||
                                                        ($_GET[id] == '24') || ($_GET[id] == '25') || ($_GET[id] == '26') || ($_GET[id] == '27')
                                                        || ($_GET[id] == '23') || ($_GET[id] == '335') || ($_GET[id] == '336')):
                                                    ?>
                                                    <?php echo ("<img alt='Νέο βιβλίο' src='/templates/abookbook_gr/images/books_icons/new_badge.png' />"); ?>
                                                <?php endif; ?>	
                                            <?php else: ?>
                                                <h2>
                                                <a href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                                    <?php echo mb_strtoupper($this->item->title); ?>
                                                </a>
                                            	</h2>
                                            <?php endif; ?>


                                            <?php if ($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
                                                <!-- Featured flag -->
                                                <span>
                                                    <sup>
                                                        <?php echo JText::_('K2_FEATURED'); ?>
                                                    </sup>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ($this->item->params->get('catItemAuthor')): ?>
                                        <!-- Item Author -->
                                        <span class="catItemAuthor">
                                            <?php echo K2HelperUtilities::writtenBy($this->item->author->profile->gender); ?> 
                                            <?php if (isset($this->item->author->link) && $this->item->author->link): ?>
                                                <a rel="author" href="<?php echo $this->item->author->link; ?>"><?php echo $this->item->author->name; ?></a>
                                            <?php else: ?>
                                                <?php echo $this->item->author->name; ?>
                                            <?php endif; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="catItemExtraFields">
                                    <ul>
                                        <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                                            <?php if ($extraField->value): ?>
                                                <div id="catAddInfo">
                                                    <ul id="rightSideCatUl">
                                                        <?php if ($extraField->name == "Συγγραφέας"): ?>
                                                            <li id="bookbookCatAuthor" class="odd"  itemprop="author">		
                                                                <?php if (strpos($extraField->value, 'href="http://"') !== false): ?>    										
                                                                    <?php
                                                                    $p = explode('&gt;', htmlspecialchars($extraField->value));
                                                                    $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                                    ?>
                                                                    <?php if ($p[1] !== "http://"): ?> 
                                                                    <strong>
                                                                        <span class="itemExtraFieldsValue"><?php print_r($p[1]); ?></span>
                                                                    </strong>    
                                                                    <?php endif; ?>			
                                                                <?php else: ?>
                                                                   <strong>
                                                                    	<span class="itemExtraFieldsValue"><?php echo $extraField->value; ?></span>	
                                                                    </strong>	
                                                                <?php endif; ?>	
                                                            </li>
                                                        <?php elseif ($extraField->name == "Εικονογράφηση"): ?>
                                                            <li id="bookbookCatEikonografos" class="odd">
                                                                <?php if (strpos($extraField->value, 'href="http://"') !== false): ?>    										
                                                                    <?php
                                                                    $p = explode('&gt;', htmlspecialchars($extraField->value));
                                                                    $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                                    ?>
                                                                    <?php if ($p[1] !== "http://"): ?> 
                                                                        <em>
                                                                        	<?php echo "εικονογράφηση:"; ?> 
                                                                        </em>
                                                                        <strong>
                                                                         	<?php print_r($p[1]); ?>
                                                                        </strong>
                                                                    <?php endif; ?>		
                                                                <?php else: ?>
                                                                    <span class="itemExtraFieldsValue">
                                                                        <?php
                                                                        echo "εικονογράφηση:";
                                                                        echo $extraField->value;
                                                                        ?>
                                                                    </span>		
                                                                <?php endif; ?>
                                                            </li>
                                                            <div class="clr"></div>
                                                        <?php elseif ($extraField->name == "Άλλο: Περιγραφή"): ?>
                                                            <li id="bookbookCatOther" class="odd">
                                                                <?php foreach ($this->item->extra_fields as $key => $extraFieldSpecial): ?>
                                                                    <?php if ($extraFieldSpecial->value): ?>
                                                                        <?php if ($extraFieldSpecial->name == "Άλλο"): ?>

                                                                            <?php if (strpos($extraFieldSpecial->value, 'href="http://"') !== false): ?>    										
                                                                                <?php
                                                                                $p = explode('&gt;', htmlspecialchars($extraFieldSpecial->value));
                                                                                $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                                                ?>	
                                                                                <?php if ($p[1] !== "http://"): ?> 										
                                                                                    <em>
                                                                                        <?php echo $extraField->value; echo ": "; ?> 
                                                                                    </em>
                                                                                    <strong>
                                                                                        <?php print_r($p[1]); ?>
                                                                                    </strong> 
                                                                                <?php endif; ?>
                                                                            <?php else: ?>
                                                                                <em>	
                                                                                    <?php
                                                                                    echo $extraField->value;
                                                                                    echo ": ";
                                                                                    ?> 
                                                                                </em>
                                                                                <strong>
                                                                                    <?php echo $extraFieldSpecial->value; ?>
                                                                                </strong>			
                                                                            <?php endif; ?>
                                                                        <?php endif; ?>			
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                            </li>
                                                            <div class="clr"></div>    
                                                        <?php elseif ($extraField->name == "Εκδότης"): ?>
                                                            <li id="bookbookCatPublisher" class="odd" itemprop="publisher">
                                                                <?php if (strpos($extraField->value, 'href="http://"') !== false): ?>    										
                                                                    <?php
                                                                    $p = explode('&gt;', htmlspecialchars($extraField->value));
                                                                    $p[1] = str_replace('&lt;/a', '', $p[1]);
                                                                    ?>
                                                                    <?php if ($p[1] !== "http://"): ?> 
                                                                        <span class="itemExtraFieldsValue"><?php print_r($p[1]); ?></span>	
                                                                    <?php endif; ?>
                                                                <?php else: ?>
                                                                    <span class="itemExtraFieldsValue"><?php echo $extraField->value; ?></span>		
                                                                <?php endif; ?>
                                                            </li>
                                                        <?php elseif ($extraField->name == "Ηλικία"): ?>
                                                            <li id="bookbookCatAge" class="odd">
                                                               <?php if($extraField->value!="Χωρίς ηλικία"):?>
                                                            		<span class="itemExtraFieldsValue"><?php echo $extraField->value; ?></span>
                                                            	<?php endif; ?>			
                                                            </li>	
                                                        <?php endif; ?>	
                                                    </ul>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                    <div class="clr"></div>
                                </div>
                            </td>
                            <?php $lineCounter = 0; ?>
                            <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                                <?php if ($extraField->value): ?>
                                    <?php
                                    if ((($extraField->name == "Η γνώμη του bookbook.gr") && ($_GET[id] != '23') && ($_GET[id] != '24') && ($_GET[id] != '25') &&
                                            ($_GET[id] != '26') && ($_GET[id] != '27') ) || ($extraField->name == "Το αγαπήσαμε") || ($extraField->name == "Δείτε το βιβλίο")
                                            || ($extraField->name == "Απόσμασμα 1")):
                                        ?>

                                        <?php
                                        if ($extraField->value != "" && $lineCounter == 0 && (htmlspecialchars($extraField->value) != '&lt;a href=&quot;http://&quot; &gt;http://&lt;/a&gt;') &&
                                                (htmlspecialchars($extraField->value) != "&lt;a href=&quot;http://&quot; class=&quot;modal&quot; rel=&quot;{handler:'iframe',size:{x:900,y:600}}&quot;&gt;http://&lt;/a&gt;") &&
                                                (htmlspecialchars($extraField->value) != "&lt;a href=&quot;http://&quot; &gt;Ξεφυλλίστε τις πρώτες σελίδες του βιβλίου&lt;/a&gt;") &&
                                                ($extraField->value != "Χωρίς ηλικία")&&
                                                ($extraField->value != "Off")&&
                                                ($extraField->value != "On")&&
                                                ($extraField->value != "Όχι")):
                                            ?>

                                            <td id="verticalLine" align="right" valign="top" rowspan="4" width="6"> 
                                            </td>
                                            <?php $lineCounter = 1; ?>
                                        <?php endif; ?>	

                                    <?php endif; ?>			
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tr>
                </table>
            </div> 

            <div id="catAddIconsActions" class="catItemExtraFields">
                <ul id="rightSideCatUl">

                    <?php if (($_GET[id] != '23') && ($_GET[id] != '24') && ($_GET[id] != '25') && ($_GET[id] != '26') && ($_GET[id] != '27')): ?>
                        <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                            <?php if ($extraField->value): ?>
                                <?php if ($extraField->name == "Η γνώμη του bookbook.gr"): ?>
                                    <?php if ($extraField->value != ""): ?>
                                        <div id="gnomiCatBookbook">
                                            <a href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>#gnomiBookBookTitle">
                                                <?php echo ("<img alt='Το διαβάσαμε' src='/templates/abookbook_gr/images/books_icons/bookbook-we-ve-read-it_100px.jpg' />"); ?>
                                            </a> 
                                        </div>	
                                    <?php endif; ?>	
                                <?php endif; ?>			
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>	


                    <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                        <?php if ($extraField->value): ?>
                            <?php if ($extraField->name == "Το αγαπήσαμε"): ?>
                                <div id="weLovedItCat">
                                    <?php if ($extraField->value == "Ναι"): ?>
                                        <a href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                            <?php echo ("<img alt='Το αγαπήσαμε' src='/templates/abookbook_gr/images/books_icons/bookbook-we-love_100px.jpg' />"); ?>		
                                        </a> 
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>			
                        <?php endif; ?>
                    <?php endforeach; ?> 


                    <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                        <?php if ($extraField->value): ?>
                            <!-- Remove name from extra fields. Shows only the value.-->
                            <!--<span class="catItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>-->
                            <?php if ($extraField->name == "Δείτε το βιβλίο"): ?>								
                                <?php
                                if ((htmlspecialchars($extraField->value) != '&lt;a href=&quot;http://&quot; &gt;http://&lt;/a&gt;') &&
                                        (htmlspecialchars($extraField->value) != "&lt;a href=&quot;http://&quot; class=&quot;modal&quot; rel=&quot;{handler:'iframe',size:{x:900,y:600}}&quot;&gt;http://&lt;/a&gt;") &&
                                        (htmlspecialchars($extraField->value) != "&lt;a href=&quot;http://&quot; &gt;Ξεφυλλίστε τις πρώτες σελίδες του βιβλίου&lt;/a&gt;")
                                ):
                                    ?>
                                    <?php if (strpos($extraField->value, 'href="http://"') == false): ?>  	
                                        <li class="odd">
                                            <?php
                                            $p = explode('&gt;', htmlspecialchars($extraField->value));
                                            $p[1] = str_replace('&lt;/a', '', $p[1]);
                                            $p[1] = htmlspecialchars('<img alt="Ξεφυλλίστε το βιβλίο" src="/templates/abookbook_gr/images/books_icons/bookbook-deite_100px.jpg" Title="Ξεφυλλίστε τις πρώτες σελίδες του βιβλίου"/>');
                                            $p[1].='&lt;/a>';
                                            echo htmlspecialchars_decode($p[0] . '>' . $p[1] . '' . $p[2]);
                                            ?>	
                                        </li>	
                                    <?php endif; ?>	
                                <?php endif; ?>							
                            <?php endif; ?>	
                        <?php endif; ?>
                    <?php endforeach; ?> 
                    <div id="bookbookWeListenCat">                            
                        <?php foreach ($this->item->extra_fields as $key => $extraField): ?>
                            <?php if ($extraField->value): ?>
                                <?php if ($extraField->name == "Απόσμασμα 1"): ?>								
                                    <?php if ((htmlspecialchars($extraField->value) != '')): ?>            						
                                        <a href="<?php echo $this->item->link; ?>&c_catid=<?php echo $_GET[id]; ?>">
                                            <img alt='Ακόυστε το βιβλίο' src="/templates/abookbook_gr/images/books_icons/listen_100px.png" />
                                        </a>
                                    <?php endif; ?>							
                                <?php endif; ?>	
                            <?php endif; ?>
                        <?php endforeach; ?> 
                    </div>
                </ul>
            </div> 
        </div>
        <div class="clr"></div>
        <div id="lineCat">l</div>


        <?php if ($this->item->params->get('catItemIntroText')): ?>
            <!-- Item introtext -->
            <div class="catItemIntroText">
                <?php echo $this->item->introtext; ?>
            </div>
        <?php endif; ?>

        <div class="clr"></div>

        <!-- Plugins: AfterDisplayContent -->
        <?php echo $this->item->event->AfterDisplayContent; ?>

        <!-- K2 Plugins: K2AfterDisplayContent -->
        <?php echo $this->item->event->K2AfterDisplayContent; ?>

        <div class="clr"></div>
    </div>
    <?php
    if (
            $this->item->params->get('catItemHits') ||
            $this->item->params->get('catItemCategory') ||
            $this->item->params->get('catItemTags') ||
            $this->item->params->get('catItemAttachments')
    ):
        ?>
        <div class="catItemLinks">

            <?php if ($this->item->params->get('catItemHits')): ?>
                <!-- Item Hits -->
                <div class="catItemHitsBlock">
                    <span class="catItemHits">
                        <?php echo JText::_('K2_READ'); ?> <b><?php echo $this->item->hits; ?></b> <?php echo JText::_('K2_TIMES'); ?>
                    </span>
                </div>
            <?php endif; ?>

            <?php if ($this->item->params->get('catItemCategory')): ?>
                <!-- Item category name -->
                <div class="catItemCategory">
                    <span><?php echo JText::_('K2_PUBLISHED_IN'); ?></span>
                    <!-- Removes the name from the original category. Keeps only the name of additional categories-->
                    <!-- <a href="<?php echo $this->item->category->link; ?>"><?php echo $this->item->category->name; ?></a> -->
                    <!-- Shows additional categories from K2 Additional categories except the main category -->
                    <?php
                    $dispatcher = JDispatcher::getInstance();
                    JPluginHelper::importPlugin('k2');
                    $additional_category_links = $dispatcher->trigger('onK2AfterLinkCategoryPublish', array($this->item->id));
                    echo $additional_category_links[0];
                    ?>
                </div>
            <?php endif; ?>

            <?php if ($this->item->params->get('catItemAttachments') && count($this->item->attachments)): ?>
                <!-- Item attachments -->
                <div class="catItemAttachmentsBlock">
                    <span><?php echo JText::_('K2_DOWNLOAD_ATTACHMENTS'); ?></span>
                    <ul class="catItemAttachments">
                        <?php foreach ($this->item->attachments as $attachment): ?>
                            <li>
                                <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>">
                                    <?php echo $attachment->title; ?>
                                </a>
                                <?php if ($this->item->params->get('catItemAttachmentsCounter')): ?>
                                    <span>(<?php echo $attachment->hits; ?> <?php echo ($attachment->hits == 1) ? JText::_('K2_DOWNLOAD') : JText::_('K2_DOWNLOADS'); ?>)</span>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <div class="clr"></div>
        </div>
    <?php endif; ?>

    <div class="clr"></div>

    <?php if ($this->item->params->get('catItemVideo') && !empty($this->item->video)): ?>
        <!-- Item video -->
        <div class="catItemVideoBlock">
            <h3><?php echo JText::_('K2_RELATED_VIDEO'); ?></h3>
            <?php if ($this->item->videoType == 'embedded'): ?>
                <div class="catItemVideoEmbedded">
                    <?php echo $this->item->video; ?>
                </div>
            <?php else: ?>
                <span class="catItemVideo"><?php echo $this->item->video; ?></span>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($this->item->params->get('catItemImageGallery') && !empty($this->item->gallery)): ?>
        <!-- Item image gallery -->
        <div class="catItemImageGallery">
            <h4><?php echo JText::_('K2_IMAGE_GALLERY'); ?></h4>
            <?php echo $this->item->gallery; ?>
        </div>
    <?php endif; ?>

    <div class="clr"></div>

    <?php if ($this->item->params->get('catItemReadMore')): ?>
        <!-- Item "read more..." link -->
        <div class="catItemReadMore">
            <a class="k2ReadMore" href="<?php echo $this->item->link; ?>">
                <?php echo JText::_('K2_READ_MORE'); ?>
            </a>
        </div>
    <?php endif; ?>

    <div class="clr"></div>

    <?php if ($this->item->params->get('catItemDateModified')): ?>
        <!-- Item date modified -->
        <?php if ($this->item->modified != $this->nullDate && $this->item->modified != $this->item->created): ?>
            <span class="catItemDateModified">
                <?php echo JText::_('K2_LAST_MODIFIED_ON'); ?> <?php echo JHTML::_('date', $this->item->modified, JText::_('K2_DATE_FORMAT_LC2')); ?>
            </span>
        <?php endif; ?>
    <?php endif; ?>

    <!-- Plugins: AfterDisplay -->
    <?php echo $this->item->event->AfterDisplay; ?>

    <!-- K2 Plugins: K2AfterDisplay -->
    <?php echo $this->item->event->K2AfterDisplay; ?>

    <div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
