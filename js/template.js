$( document ).ready(function() {
    var $wrapper = $(".wrapper")
    var $body = $("body")
    var $sidenavOverlay = $("#sidenav-overlay")

    var toggleSidenav = function () {
        $wrapper.toggleClass("toggled");
        $body.toggleClass("noscroll");
        $sidenavOverlay.toggleClass("hidden-animated");
    }

    $("#menu-toggle").click(function(e) {
    	toggleSidenav();
     });

    $sidenavOverlay.on('click' , function () {
    	toggleSidenav();
    });


    // Touch events
    var dragTarget = new Hammer.Manager(document.getElementById("drag-target"));
    dragTarget.add( new Hammer.Pan({ threshold: 200 }) );
    dragTarget.on("panend", function(ev) {
        if (ev.direction == 4) {
        	toggleSidenav();
        }
    });

    var overlay = new Hammer.Manager(document.getElementById("sidenav-overlay"));
    overlay.add( new Hammer.Pan({ threshold: 60 }) );
    overlay.on("panend", function(ev) {
    	if (ev.direction == 2) {
    		toggleSidenav();
    	}
    });


    // copied from the main site

    // add the clicked class to the ages menu
    if ($('#k2Container').hasClass('itemListView prosxolika-class')) {
        document.getElementById("prosxolikiA").className="giaProsxolikiHlikiaHrefClicked";
    }
    else if($('#k2Container').hasClass('itemListView penteEfta-class')){
        document.getElementById("penteEftaA").className="giaPenteEftaHrefClicked";
    }
    else if($('#k2Container').hasClass('itemListView eftaEnnia-class')){
        document.getElementById("eftaEnniaA").className="giaEftaEnniaHrefClicked";
    }
    else if($('#k2Container').hasClass('itemListView enniaDodeka-class')){
        document.getElementById("enniaDodekaA").className="giaEnniaDodekaHrefClicked";
    }else if ($('#k2Container').hasClass('itemListView efivous-class')){
        document.getElementById("efivousA").className="giaEfivousHrefClicked";
    }
    else if ($('#k2Container').hasClass('itemListView giaoOliTinOikogeneia-class')){
        document.getElementById("giaOliTinOikogeneiaA").className="giaOliTinOikogeneiaClicked";
    }
});