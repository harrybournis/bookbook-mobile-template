<?php
  defined('_JEXEC') or die; ?>

<header data-spy="affix" data-offset-top="80" data-offset-bottom="200">
  <button type="button" class="btn btn-default" id="menu-toggle">
    <i class="fa fa-bars"></i>
  </button>
  <span class="navbar-logo-container">
    <a href="/" class="navbar-logo-link">
      <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/mobile_version/bookbook-mobile_logo.png" class="img-responsive" id="navbar-logo">
    </a>
  </span>
  <span class="spacer"></span>
  <div class="right-menu">
<!--     <a href="#"><i class="fa fa-key"></i></a>
    <a href="#"><i class="fa fa-search"></i></a>
    <a href="#"><i class="fa fa-envelope"></i></a> -->
    <a href="/login"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/mobile_version/bookbook-mobile_login.png"></a>
    <a href="/search-button"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/mobile_version/bookbook-mobile_search.png"></a>
    <a href="/contact"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/mobile_version/bookbook-mobile_contact.png"></a>
  </div>
</header>
