<?php
/// no direct access
defined('_JEXEC') or die;

include 'includes/unseting.php';

	$dayForBackground = date('N');
	switch ($dayForBackground) {
		case 1:
			$color = '#000000';
			$background_color = '#cc0000';
			break;
		case 2:
			$color = '#000000';
			$background_color = '#e1c300';
			break;
		case 3:
			$color = '#000000';
			$background_color = '#ca511e';
			break;
		case 4:
			$color = '#000000';
			$background_color = '#966cc9';
			break;
		case 5:
			$color = '#000000';
			$background_color = '#a9b828';
			break;
		case 6:
			$color = '#000000';
			$background_color = '#8f8fb3';
			break;
		case 7:
			$color = '#000000';
			$background_color = '#c1c014';
			break;

	}
?>

<head>
	<jdoc:include type="head" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template.min.css" type="text/css" />
	<link rel="apple-touch-icon-precomposed"                 href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72"   href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/apple-touch-icon-144x144-precomposed.png">

	<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/hammer.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/hammer-time.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/template.min.js"></script>

	<!-- Change colour depending on the day. Only on larger than 1024 -->
	<style type="text/css">
		@media (min-width: 1200px) {
	    body {
				margin: 0;
				color : <?php echo $color; ?>;
				background-color: <?php echo $background_color; ?>;
			}
    }
		@media(max-width: 1199px) {
			#page-content-wrapper {
				color : <?php echo $color; ?>;
				background-color: <?php echo $background_color; ?>;
			}
		}
  </style>
</head>
